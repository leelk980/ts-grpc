import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { ProtoGrpcType as CompanyProtoGrpcType } from './proto/company';
import { ProtoGrpcType as UserProtoGrpcType } from './proto/user';

const PORT = 8082;
const COMPANY_PROTO_FILE = '/proto/company.proto';
const USER_PROTO_FILE = '/proto/user.proto';

const companyPackageDef = protoLoader.loadSync(__dirname + COMPANY_PROTO_FILE);
const userPackageDef = protoLoader.loadSync(__dirname + USER_PROTO_FILE);

const companyGrpcObj = grpc.loadPackageDefinition(companyPackageDef) as unknown as CompanyProtoGrpcType;
const userGrpcObj = grpc.loadPackageDefinition(userPackageDef) as unknown as UserProtoGrpcType;

const companyPackage = companyGrpcObj.CompanyPackage;
const userPackage = userGrpcObj.UserPackage;

const companyClient = new companyPackage.company(`0.0.0.0:${PORT}`, grpc.credentials.createInsecure());
const userClient = new userPackage.user(`0.0.0.0:${PORT}`, grpc.credentials.createInsecure());

const deadline = new Date();
deadline.setSeconds(deadline.getSeconds() + 5);
companyClient.waitForReady(deadline, (err) => {
  if (err) {
    console.error('company err ' + err);
    return;
  }
  companyFn();
});

userClient.waitForReady(deadline, (err) => {
  if (err) {
    console.error('user err ' + err);
    return;
  }
  userFn();
});

const companyFn = () =>
  companyClient.findCompanyById({ companyId: '1' }, (err, result) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(result);
  });

const userFn = () =>
  userClient.findUserById({ userId: '1' }, (err, result) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(result);
  });
