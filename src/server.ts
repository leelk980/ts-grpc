import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { ProtoGrpcType as CompanyProtoGrpcType } from './proto/company';
import { ProtoGrpcType as UserProtoGrpcType } from './proto/user';

const PORT = 8082;
const COMPANY_PROTO_FILE = '/proto/company.proto';
const USER_PROTO_FILE = '/proto/user.proto';

const companyPackageDef = protoLoader.loadSync(__dirname + COMPANY_PROTO_FILE);
const userPackageDef = protoLoader.loadSync(__dirname + USER_PROTO_FILE);

const companyGrpcObj = grpc.loadPackageDefinition(companyPackageDef) as unknown as CompanyProtoGrpcType;
const userGrpcObj = grpc.loadPackageDefinition(userPackageDef) as unknown as UserProtoGrpcType;

const companyPackage = companyGrpcObj.CompanyPackage;
const userPackage = userGrpcObj.UserPackage;

function main() {
  const server = getServer();

  server.bindAsync(`localhost:${PORT}`, grpc.ServerCredentials.createInsecure(), (err, port) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(`Your server as started on port ${port}`);
    server.start();
  });
}

function getServer() {
  const server = new grpc.Server();
  server.addService(companyPackage.company.service, {
    findCompanyById: (req: any, res: any) => res(null, { company: 'find company' }),
  });

  server.addService(userPackage.user.service, {
    findUserById: (req: any, res: any) => res(null, { user: 'find user' }),
  });

  return server;
}

main();
