import type * as grpc from '@grpc/grpc-js';
import type { MessageTypeDefinition } from '@grpc/proto-loader';

import type { userClient as _UserPackage_userClient, userDefinition as _UserPackage_userDefinition } from './UserPackage/user';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  UserPackage: {
    findUserByIdRequest: MessageTypeDefinition
    findUserByIdResponse: MessageTypeDefinition
    user: SubtypeConstructor<typeof grpc.Client, _UserPackage_userClient> & { service: _UserPackage_userDefinition }
  }
}

