// Original file: src/proto/company.proto


export interface findCompanyByIdRequest {
  'companyId'?: (string);
}

export interface findCompanyByIdRequest__Output {
  'companyId': (string);
}
