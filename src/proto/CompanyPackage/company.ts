// Original file: src/proto/company.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { findCompanyByIdRequest as _CompanyPackage_findCompanyByIdRequest, findCompanyByIdRequest__Output as _CompanyPackage_findCompanyByIdRequest__Output } from '../CompanyPackage/findCompanyByIdRequest';
import type { findCompanyByIdResponse as _CompanyPackage_findCompanyByIdResponse, findCompanyByIdResponse__Output as _CompanyPackage_findCompanyByIdResponse__Output } from '../CompanyPackage/findCompanyByIdResponse';

export interface companyClient extends grpc.Client {
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, metadata: grpc.Metadata, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, metadata: grpc.Metadata, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findCompanyById(argument: _CompanyPackage_findCompanyByIdRequest, callback: (error?: grpc.ServiceError, result?: _CompanyPackage_findCompanyByIdResponse__Output) => void): grpc.ClientUnaryCall;
  
}

export interface companyHandlers extends grpc.UntypedServiceImplementation {
  findCompanyById: grpc.handleUnaryCall<_CompanyPackage_findCompanyByIdRequest__Output, _CompanyPackage_findCompanyByIdResponse>;
  
}

export interface companyDefinition extends grpc.ServiceDefinition {
  findCompanyById: MethodDefinition<_CompanyPackage_findCompanyByIdRequest, _CompanyPackage_findCompanyByIdResponse, _CompanyPackage_findCompanyByIdRequest__Output, _CompanyPackage_findCompanyByIdResponse__Output>
}
