import type * as grpc from '@grpc/grpc-js';
import type { MessageTypeDefinition } from '@grpc/proto-loader';

import type { companyClient as _CompanyPackage_companyClient, companyDefinition as _CompanyPackage_companyDefinition } from './CompanyPackage/company';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  CompanyPackage: {
    company: SubtypeConstructor<typeof grpc.Client, _CompanyPackage_companyClient> & { service: _CompanyPackage_companyDefinition }
    findCompanyByIdRequest: MessageTypeDefinition
    findCompanyByIdResponse: MessageTypeDefinition
  }
}

