// Original file: src/proto/user.proto


export interface findUserByIdRequest {
  'userId'?: (string);
}

export interface findUserByIdRequest__Output {
  'userId': (string);
}
