// Original file: src/proto/user.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { findUserByIdRequest as _UserPackage_findUserByIdRequest, findUserByIdRequest__Output as _UserPackage_findUserByIdRequest__Output } from '../UserPackage/findUserByIdRequest';
import type { findUserByIdResponse as _UserPackage_findUserByIdResponse, findUserByIdResponse__Output as _UserPackage_findUserByIdResponse__Output } from '../UserPackage/findUserByIdResponse';

export interface userClient extends grpc.Client {
  findUserById(argument: _UserPackage_findUserByIdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, metadata: grpc.Metadata, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, metadata: grpc.Metadata, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, options: grpc.CallOptions, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  findUserById(argument: _UserPackage_findUserByIdRequest, callback: (error?: grpc.ServiceError, result?: _UserPackage_findUserByIdResponse__Output) => void): grpc.ClientUnaryCall;
  
}

export interface userHandlers extends grpc.UntypedServiceImplementation {
  findUserById: grpc.handleUnaryCall<_UserPackage_findUserByIdRequest__Output, _UserPackage_findUserByIdResponse>;
  
}

export interface userDefinition extends grpc.ServiceDefinition {
  findUserById: MethodDefinition<_UserPackage_findUserByIdRequest, _UserPackage_findUserByIdResponse, _UserPackage_findUserByIdRequest__Output, _UserPackage_findUserByIdResponse__Output>
}
